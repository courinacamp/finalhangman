import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.*;
import java.io.IOException;


public class Hangman
{   
    String currentFamily = "";
    HashMap<String, ArrayList<String>> map;
    ArrayList<String> wordList;
    String currentGuess;
    
    public Hangman(String dictionary, int wordLength)
    {
        String fileName = dictionary;
        wordList = new ArrayList<String>();
       
        File f = new File(fileName);
        try
        {
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            
            String line;
            line = br.readLine();
            while (line != null)
            {
                if(line.length() == wordLength)
                {
                    wordList.add(line);
                }
                line = br.readLine();
            }
        }
        catch (java.io.FileNotFoundException fnfe)
        {
            System.out.println("File not found: " + dictionary);
            System.exit(1);
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("Error, something went wrong");
            System.exit(1);
        }
        
        
        for(int i = 0; i < wordLength; i++)
        {
           currentFamily += "-"; 
        }
        
    }
    
    public void printArray()
    {
        System.out.println(wordList);
    }
    
    public String getCurrentFamily()
    {
        return currentFamily;
    }
       
    public void makeGuess(char g)
    {
        currentGuess = "" + g;
        String currentWord;
        String temp = "";
        int indexOfGuess;
        //
        for(int i = 0; i < wordList.size(); i++)
        {
            currentWord = wordList.get(i);
            
            if(currentWord.contains(currentGuess))
            {
                for(int k = 0; k < currentWord.length(); k++)
                {
                    if(currentWord.charAt(k) == g)
                    {
                        temp = temp + g;
                    }
                    else
                    {
                        temp = temp + "-";
                    }
                }
                
            }
            
            //Check if the map contains the key already
            //If it does then add the value to the array list
            //if not add the key and value
            if(!map.containsKey(temp))
            {
                ArrayList<String> newFamily;
                newFamily = new ArrayList<String>();
                newFamily.add(wordList.get(i));
                map.put(temp, newFamily);
                temp = "";
            }
            else if(map.containsKey(temp))
            {
                ArrayList<String> newFamily = new ArrayList<String>();
                newFamily = map.get(temp);
                newFamily.add(wordList.get(i));
                temp = "";
            }
        }
        
        Set<String> keys = map.keySet();
            
        int longest = 0;
        String longestFamily = currentFamily;
            
        for (String key : keys)
        {
            if (map.get(key).size() > longest)
            {
                longest = map.get(key).size();
                longestFamily = key;
            }
        }
            
        currentFamily = longestFamily;
        wordList = map.get(longestFamily);
        map.clear();
    }
}
