import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 * Write a description of JavaFX class Main here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Main extends Application
{
    // We keep track of the count, and label displaying the count:
    private int count = 0;
    
    Scene gameScene, menuScene;
    Stage gameWindow;
    TextField lengthField, dictionaryField, guessesField;
    TextField guessField;
    Label turnsLeft, family;
    Hangman hangman;

    /**
     * The start method is the main entry point for every JavaFX application. 
     * It is called after the init() method has returned and after 
     * the system is ready for the application to begin running.
     *
     * @param  stage the primary stage for this application.
     */
    @Override
    public void start(Stage stage)
    {
        // Save value of Stage
        gameWindow = stage;
        
        // Create a Button or any control item
        Button startButton = new Button("Start");
        
        // Labels
        Label lengthLabel = new Label("Word length:");
        Label dictionaryLabel = new Label("Dictionary:");
        Label guessesLabel = new Label("Guesses:");
        
        // Text Fields
        lengthField = new TextField("5");
        dictionaryField = new TextField("dictionary1000.txt");
        guessesField = new TextField("20");

        // Create a new grid pane
        GridPane pane = new GridPane();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setMinSize(300, 300);
        pane.setVgap(10);
        pane.setHgap(10);
        
        // Add the button and label into the pane
        pane.add(lengthLabel, 0, 0);
        pane.add(lengthField, 1, 0);
        pane.add(dictionaryLabel, 0, 1);
        pane.add(dictionaryField, 1, 1);
        pane.add(guessesLabel, 0, 2);
        pane.add(guessesField, 1, 2);
        pane.add(startButton, 0, 3);

        //set an action on the button using method reference
        startButton.setOnAction(this::buttonClick);



        // JavaFX must have a Scene (window content) inside a Stage (window)
        menuScene = new Scene(pane, 400,300);
        stage.setTitle("Evil Hangman");
        stage.setScene(menuScene);
        
        // The game scene
        Pane gamePane = new Pane();
        gameScene = new Scene(gamePane, 400, 300);
        Label turnsLabel = new Label("Turns: ");
        Label guessLabel = new Label("Guess a char: ");
        Button goButton = new Button("Go");
        family = new Label("");
        guessField = new TextField();
        turnsLeft = new Label("");
        
        gamePane.getChildren().add(turnsLabel);
        turnsLabel.relocate(20, 20);
        gamePane.getChildren().add(turnsLeft);
        turnsLeft.relocate(100, 20);
        gamePane.getChildren().add(family);
        family.relocate(100, 50);
        gamePane.getChildren().add(guessLabel);
        guessLabel.relocate(20, 150);
        gamePane.getChildren().add(guessField);
        guessField.relocate(20, 200);
        gamePane.getChildren().add(goButton);
        goButton.relocate(20, 250);
        goButton.setOnAction(this::handleGoButton);
        

        // Show the Stage (window)
        stage.show();
    }

    /**
     * This will be executed when the button is clicked
     * It increments the count by 1
     */
    private void buttonClick(ActionEvent event)
    {
        gameWindow.setScene(gameScene);
        String lengthStr = lengthField.getText();
        int length = Integer.parseInt(lengthStr);
        hangman = new Hangman(dictionaryField.getText(), length);
        turnsLeft.setText(guessesField.getText());
        family.setText(hangman.getCurrentFamily());
        count = Integer.parseInt(turnsLeft.getText());
    }
    
    /**
     * Handler for goButton
     */
    private void handleGoButton(ActionEvent event)
    {
        String guess = guessField.getText();
        hangman.makeGuess(guess.charAt(0));
        
        // To be completed by you
    }
}